package ru.blogic.demo.server;

import org.junit.Test;
import ru.blogic.demo.client.service.GWTService;
import ru.blogic.demo.shared.TaskStatusEnum;

import static org.junit.Assert.*;

public class GWTServiceImplTest {

    private GWTService gwtService = new GWTServiceImpl();

    @Test
    public void addTask() {
        gwtService.addTask("AAAAA", "aaaaa");
    }

    @Test
    public void changeStatus() {
        gwtService.changeStatus(new Long(1), TaskStatusEnum.COMPLETED.getIdTaskStatus());
    }

    @Test
    public void getAllTask() {
        gwtService.getAllTask();
    }

    @Test
    public void getTaskByStatus() {
        gwtService.getTaskByStatus(TaskStatusEnum.COMPLETED.getIdTaskStatus());
    }

    @Test
    public void getTaskById() {
        gwtService.getTaskById(new Long(1));
    }
}