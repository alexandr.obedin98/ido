package ru.blogic.demo.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.blogic.demo.shared.TaskDTO;

import java.util.List;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GWTServiceAsync {

  void addTask(String title, String description, AsyncCallback<Long> async);

  void changeStatus(Long idTask, int idTaskStatus, AsyncCallback<Long> async);

  void getAllTask(AsyncCallback<List<TaskDTO>> async);

  void getTaskByStatus(int idTaskStatus, AsyncCallback<List<TaskDTO>> async);

  void getTaskById(Long idTask, AsyncCallback<List<TaskDTO>> async);
}
