package ru.blogic.demo.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.blogic.demo.shared.TaskDTO;

import java.util.List;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("gwt")
public interface GWTService extends RemoteService {

  Long addTask(String title, String description);

  Long changeStatus(Long idTask, int idTaskStatus);

  List<TaskDTO> getAllTask();

  List<TaskDTO> getTaskByStatus(int idTaskStatus);

  List<TaskDTO> getTaskById(Long idTask);
}
