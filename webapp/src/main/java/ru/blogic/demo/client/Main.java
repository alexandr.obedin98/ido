package ru.blogic.demo.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.vaadin.polymer.elemental.*;
import com.vaadin.polymer.paper.*;
import ru.blogic.demo.client.service.GWTService;
import ru.blogic.demo.client.service.GWTServiceAsync;
import ru.blogic.demo.shared.TaskDTO;
import ru.blogic.demo.shared.TaskStatusEnum;

import java.util.ArrayList;
import java.util.List;

public class Main extends Composite {

    private GWTServiceAsync gwtSvc = GWT.create(GWTService.class);

    interface MainUiBinder extends UiBinder<HTMLPanel, Main> {
    }

    private static MainUiBinder ourUiBinder = GWT.create(MainUiBinder.class);

    @UiField
    PaperDrawerPanelElement drawerPanel;

    @UiField
    PaperIconItemElement menuSelectAll;
    @UiField
    PaperIconItemElement menuCompleted;
    @UiField
    PaperIconItemElement menuArchived;
    @UiField
    PaperIconItemElement menuEdit;
    @UiField
    PaperIconItemElement menuDelete;

    @UiField
    PaperIconItemElement menuViewAll;
    @UiField
    PaperIconItemElement menuViewCompleted;
    @UiField
    PaperIconItemElement menuViewArchived;

    @UiField
    HTMLElement content;
    @UiField
    PaperFabElement addButton;

    @UiField
    PaperDialogElement addItemDialog;
    @UiField
    PaperInputElement titleInput;
    @UiField
    PaperTextareaElement descriptionInput;
    @UiField
    PaperButtonElement confirmAddButton;

    private List<Item> items = new ArrayList<>();

    private boolean switchFlag = false;

    public Main() {
        initWidget(ourUiBinder.createAndBindUi(this));

        addButton.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                addItemDialog.open();
            }
        });

        confirmAddButton.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                if (!titleInput.getValue().isEmpty()) {

                    // Set up the callback object.
                    AsyncCallback<Long> callback = new AsyncCallback<Long>() {
                        public void onFailure(Throwable caught) {
                            // TODO: Do something with errors.
                        }

                        @Override
                        public void onSuccess(Long aLong) {
                            // Set up the callback object.
                            AsyncCallback<List<TaskDTO>> callback = new AsyncCallback<List<TaskDTO>>() {
                                public void onFailure(Throwable caught) {
                                    // TODO: Do something with errors.
                                }

                                @Override
                                public void onSuccess(List<TaskDTO> taskDTOS) {
                                    for (TaskDTO task : taskDTOS) {
                                        Item item = new Item();
                                        item.setTitle(task.getTitle());
                                        item.setDescription(task.getTitle());
                                        item.setTask(task);
                                        content.appendChild(item.getElement());
                                        items.add(item);
                                    }
                                }
                            };

                            // Make the call to the service.
                            gwtSvc.getTaskById(aLong, callback);
                        }
                    };

                    // Make the call to the service.
                    gwtSvc.addTask(titleInput.getValue(), descriptionInput.getValue(), callback);

                    // clear text fields
                    titleInput.setValue("");
                    descriptionInput.setValue("");
                }
            }
        });

        menuSelectAll.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                if (switchFlag) {
                    for (Item item : items) {
                        if (item.isDone()) {
                            item.setDone(false);
                        }
                    }
                    switchFlag = false;
                } else {
                    for (Item item : items) {
                        if (!item.isDone()) {
                            item.setDone(true);
                        }
                    }
                    switchFlag = true;
                }
            }
        });

        menuCompleted.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                // Set up the callback object.
                AsyncCallback<Long> callback = new AsyncCallback<Long>() {
                    public void onFailure(Throwable caught) {
                        // TODO: Do something with errors.
                    }

                    @Override
                    public void onSuccess(Long aLong) {

                    }
                };

                for (Item item : items) {
                    if (item.isDone()) {
                        // Make the call to the service.
                        gwtSvc.changeStatus(new Long(1), TaskStatusEnum.COMPLETED.getIdTaskStatus(), callback);
                        content.removeChild(item.getElement());
                        items.remove(item);
                    }
                }
            }
        });

        menuArchived.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                // Set up the callback object.
                AsyncCallback<Long> callback = new AsyncCallback<Long>() {
                    public void onFailure(Throwable caught) {
                        // TODO: Do something with errors.
                    }

                    @Override
                    public void onSuccess(Long aLong) {

                    }
                };

                for (Item item : items) {
                    if (item.isDone()) {
                        // Make the call to the service.
                        gwtSvc.changeStatus(new Long(1), TaskStatusEnum.ARCHIVED.getIdTaskStatus(), callback);
                        content.removeChild(item.getElement());
                        items.remove(item);
                    }
                }
            }
        });

        menuEdit.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {

            }
        });

        menuDelete.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                closeMenu();
                // Set up the callback object.
                AsyncCallback<Long> callback = new AsyncCallback<Long>() {
                    public void onFailure(Throwable caught) {
                        // TODO: Do something with errors.
                    }

                    @Override
                    public void onSuccess(Long aLong) {

                    }
                };

                for (Item item : items) {
                    if (item.isDone()) {
                        // Make the call to the service.
                        gwtSvc.changeStatus(new Long(1), TaskStatusEnum.DELETED.getIdTaskStatus(), callback);
                        content.removeChild(item.getElement());
                        items.remove(item);
                    }
                }
            }
        });

        menuViewAll.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                closeMenu();
                // Set up the callback object.
                AsyncCallback<List<TaskDTO>> callback = new AsyncCallback<List<TaskDTO>>() {
                    public void onFailure(Throwable caught) {
                        // TODO: Do something with errors.
                    }

                    @Override
                    public void onSuccess(List<TaskDTO> taskDTOS) {
                        for (TaskDTO task : taskDTOS) {
                            Item item = new Item();
                            item.setTitle(task.getTitle());
                            item.setDescription(task.getTitle());
                            item.setTask(task);
                            content.appendChild(item.getElement());
                            items.add(item);
                        }
                    }
                };

                // Make the call to the service.
                gwtSvc.getAllTask(callback);
            }
        });

        menuViewCompleted.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                closeMenu();
                // Set up the callback object.
                AsyncCallback<List<TaskDTO>> callback = new AsyncCallback<List<TaskDTO>>() {
                    public void onFailure(Throwable caught) {
                        // TODO: Do something with errors.
                    }

                    @Override
                    public void onSuccess(List<TaskDTO> taskDTOS) {
                        for (TaskDTO task : taskDTOS) {
                            Item item = new Item();
                            item.setTitle(task.getTitle());
                            item.setDescription(task.getTitle());
                            item.setTask(task);
                            content.appendChild(item.getElement());
                            items.add(item);
                        }
                    }
                };

                // Make the call to the service.
                gwtSvc.getTaskByStatus(TaskStatusEnum.COMPLETED.getIdTaskStatus(), callback);
            }
        });

        menuViewArchived.addEventListener("click", new EventListener() {
            public void handleEvent(Event event) {
                closeMenu();
                // Set up the callback object.
                AsyncCallback<List<TaskDTO>> callback = new AsyncCallback<List<TaskDTO>>() {
                    public void onFailure(Throwable caught) {
                        // TODO: Do something with errors.
                    }

                    @Override
                    public void onSuccess(List<TaskDTO> taskDTOS) {
                        for (TaskDTO task : taskDTOS) {
                            Item item = new Item();
                            item.setTitle(task.getTitle());
                            item.setDescription(task.getTitle());
                            item.setTask(task);
                            content.appendChild(item.getElement());
                            items.add(item);
                        }
                    }
                };

                // Make the call to the service.
                gwtSvc.getTaskByStatus(TaskStatusEnum.ARCHIVED.getIdTaskStatus(), callback);
            }
        });
    }

    private void closeMenu() {
        if (drawerPanel.getNarrow()) {
            drawerPanel.closeDrawer();
        }
    }
}