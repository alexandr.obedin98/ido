package ru.blogic.demo.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.blogic.demo.client.service.GWTService;
import ru.blogic.demo.shared.TaskDTO;
import ru.blogic.demo.task.model.TaskModel;
import ru.blogic.demo.task.service.TaskService;

import javax.ejb.EJB;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GWTServiceImpl extends RemoteServiceServlet implements GWTService {

    @EJB
    private TaskService service;

    @Override
    public Long addTask(String title, String description) {
        Long res = null;
        try {
            res = service.addTask(title, description);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Long changeStatus(Long idTask, int idTaskStatus) {
        Long res = null;
        try {
            res = service.changeStatus(idTask, idTaskStatus);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public List<TaskDTO> getAllTask() {
        List<TaskDTO> res = new ArrayList<>();
        try {
            List<TaskModel> tasks = service.getAllTask();
            if (tasks != null) {
                for (TaskModel t : tasks) {
                    res.add(new TaskDTO(t.getIdTask(), t.getTitle(), t.getDescription(), t.getIdTaskStatus(), t.getIdPerson()));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public List<TaskDTO> getTaskByStatus(int idTaskStatus) {
        List<TaskDTO> res = new ArrayList<>();
        try {
            List<TaskModel> tasks = service.getTaskByStatus(idTaskStatus);
            if (tasks != null) {
                for (TaskModel t : tasks) {
                    res.add(new TaskDTO(t.getIdTask(), t.getTitle(), t.getDescription(), t.getIdTaskStatus(), t.getIdPerson()));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public List<TaskDTO> getTaskById(Long idTask) {
        List<TaskDTO> res = new ArrayList<>();
        try {
            List<TaskModel> tasks = service.getTaskById(idTask);
            if (tasks != null) {
                for (TaskModel t : tasks) {
                    res.add(new TaskDTO(t.getIdTask(), t.getTitle(), t.getDescription(), t.getIdTaskStatus(), t.getIdPerson()));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
}
