package ru.blogic.demo.shared;

import java.io.Serializable;

public class TaskDTO implements Serializable {

    private Long idTask;
    private String title;
    private String description;
    private int idTaskStatus;
    private String idPerson;

    public TaskDTO(Long idTask, String title, String description, int idTaskStatus, String idPerson) {
        this.idTask = idTask;
        this.title = title;
        this.description = description;
        this.idTaskStatus = idTaskStatus;
        this.idPerson = idPerson;
    }

    public TaskDTO() {
    }

    public Long getIdTask() {
        return idTask;
    }

    public void setIdTask(Long idTask) {
        this.idTask = idTask;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdTaskStatus() {
        return idTaskStatus;
    }

    public void setIdTaskStatus(int idTaskStatus) {
        this.idTaskStatus = idTaskStatus;
    }

    public String getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(String idPerson) {
        this.idPerson = idPerson;
    }
}
