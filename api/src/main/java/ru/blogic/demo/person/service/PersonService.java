package ru.blogic.demo.person.service;

import ru.blogic.demo.person.model.PersonModel;

public interface PersonService {

    PersonModel getCurrentPerson();

}
