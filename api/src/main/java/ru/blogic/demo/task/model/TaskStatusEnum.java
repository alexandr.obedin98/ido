package ru.blogic.demo.task.model;

public enum TaskStatusEnum {

    CREATED ("Создан", 0),
    COMPLETED ("Выполнен", 1),
    ARCHIVED ("Архив", 2),
    DELETED ("Удален", 3);

    private String title;
    private int idTaskStatus;

    TaskStatusEnum(String title, int idTaskStatus) {

        this.title = title;
        this.idTaskStatus = idTaskStatus;

    }

    public String getTitle() {
        return title;
    }

    public int getIdTaskStatus() {
        return idTaskStatus;
    }

    public static boolean checkStatus(int idTaskStatus) {

        for (TaskStatusEnum taskStatus : TaskStatusEnum.values()) {
            if (idTaskStatus == taskStatus.idTaskStatus) {
                return true;
            }
        }
        return false;

    }

}