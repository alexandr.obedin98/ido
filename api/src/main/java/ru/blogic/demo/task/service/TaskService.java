package ru.blogic.demo.task.service;

import ru.blogic.demo.task.model.TaskModel;

import java.sql.SQLException;
import java.util.List;

public interface TaskService {

    Long addTask(String title, String description) throws SQLException;

    Long changeStatus(Long idTask, int idTaskStatus) throws SQLException;

    List<TaskModel> getAllTask() throws SQLException;

    List<TaskModel> getTaskByStatus(int idTaskStatus) throws SQLException;

    List<TaskModel> getTaskById(Long idTask) throws SQLException;

}
