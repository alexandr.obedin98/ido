package ru.blogic.demo.task.service.impl;

import ru.blogic.demo.person.service.PersonService;
import ru.blogic.demo.task.manager.TaskManager;
import ru.blogic.demo.task.model.TaskModel;
import ru.blogic.demo.task.model.TaskStatusEnum;
import ru.blogic.demo.task.service.TaskService;

import java.sql.SQLException;
import java.util.List;

public class TaskServiceImpl implements TaskService {

    private TaskManager taskManager;
    private PersonService personService;

    public TaskServiceImpl(TaskManager taskManager, PersonService personService) {
        this.taskManager = taskManager;
        this.personService = personService;
    }

    @Override
    public Long addTask(String title, String description) throws SQLException {
        if (title == null || title.equals("")) {
            throw new RuntimeException("Нельзя создать пустую задачу");
        } else {
            Long idPerson = personService.getCurrentPerson().getIdPerson();
            return taskManager.addTask(title, description, idPerson);
        }
    }

    @Override
    public Long changeStatus(Long idTask, int idTaskStatus) throws SQLException {

        if (TaskStatusEnum.checkStatus(idTaskStatus)) {
            Long idPerson = personService.getCurrentPerson().getIdPerson();
            return taskManager.changeStatus(idTask, idTaskStatus, idPerson);
        } else {
            throw new RuntimeException("Статуса не существует, попробуйте другой");
        }

    }

    @Override
    public List<TaskModel> getAllTask() throws SQLException {

        Long idPerson = personService.getCurrentPerson().getIdPerson();
        return taskManager.getAllTask(idPerson);

    }

    @Override
    public List<TaskModel> getTaskByStatus(int idTaskStatus) throws SQLException {

        if (TaskStatusEnum.checkStatus(idTaskStatus)) {
            Long idPerson = personService.getCurrentPerson().getIdPerson();
            return taskManager.getTaskByStatus(idTaskStatus, idPerson);
        } else {
            throw new RuntimeException("Статуса не существует, попробуйте другой");
        }

    }

    @Override
    public List<TaskModel> getTaskById(Long idTask) throws SQLException {
        if (idTask == null) {
            throw new RuntimeException("Некорректный ID");
        } else {
            Long idPerson = personService.getCurrentPerson().getIdPerson();
            return taskManager.getTaskById(idTask, idPerson);
        }
    }
}
