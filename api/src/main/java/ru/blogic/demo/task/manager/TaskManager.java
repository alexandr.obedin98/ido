package ru.blogic.demo.task.manager;

import ru.blogic.demo.task.model.TaskModel;

import java.sql.SQLException;
import java.util.List;

public interface TaskManager {

    Long addTask(String title, String description, Long idPerson) throws SQLException;

    Long changeStatus(Long idTask, int idTaskStatus, Long idPerson) throws SQLException;

    List<TaskModel> getAllTask(Long idPerson) throws SQLException;

    List<TaskModel> getTaskByStatus(int idTaskStatus, Long idPerson) throws SQLException;

    List<TaskModel> getTaskById(Long idTask, Long idPerson) throws SQLException;
}
