package ru.blogic.demo.task.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.blogic.demo.person.model.PersonModel;
import ru.blogic.demo.person.service.PersonService;
import ru.blogic.demo.task.manager.TaskManager;
import ru.blogic.demo.task.model.TaskModel;
import ru.blogic.demo.task.model.TaskStatusEnum;
import ru.blogic.demo.task.service.TaskService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskServiceImplTest {

    private TaskService taskService;
    private PersonService personService;
    private TaskManager taskManager;

    @Before
    public void setUp() throws SQLException {
        PersonModel person = new PersonModel();
        person.setIdPerson(new Long(1));

        TaskModel task = new TaskModel();

        List<TaskModel> taskList = new ArrayList<>();
        taskList.add(task);

        personService = Mockito.mock(PersonService.class);
        Mockito.when(personService.getCurrentPerson()).thenReturn(person);
        taskManager = Mockito.mock(TaskManager.class);
        Mockito.when(taskManager.addTask("Лечь под поезд", null, person.getIdPerson())).thenReturn(new Long(1));
        Mockito.when(taskManager.changeStatus(new Long(1), TaskStatusEnum.CREATED.getIdTaskStatus(), person.getIdPerson())).thenReturn(new Long(1));
        Mockito.when(taskManager.getTaskByStatus(TaskStatusEnum.CREATED.getIdTaskStatus(), person.getIdPerson())).thenReturn(taskList);
        Mockito.when(taskManager.getTaskById(new Long(1), person.getIdPerson())).thenReturn(taskList);
        taskService = new TaskServiceImpl(taskManager, personService);
    }

    @Test(expected = RuntimeException.class)
    public void addNullTaskException() throws SQLException {
        taskService.addTask(null, null);
    }

    @Test(expected = RuntimeException.class)
    public void addEmptyTaskException() throws SQLException {
        taskService.addTask("", null);
    }

    @Test
    public void addTask() throws SQLException {
        Assert.assertNotNull(taskService.addTask("Лечь под поезд", null));
    }

    @Test(expected = RuntimeException.class)
    public void changeStatusException() throws SQLException {
        taskService.changeStatus(new Long(1), 100);
    }

    @Test
    public void changeStatus() throws SQLException {
        Assert.assertNotNull(taskService.changeStatus(new Long(1), TaskStatusEnum.CREATED.getIdTaskStatus()));
    }

    @Test(expected = RuntimeException.class)
    public void getTaskByStatusException() throws SQLException {
        taskService.getTaskByStatus(100);
    }

    @Test
    public void getTaskByStatus() throws SQLException {
        Assert.assertNotNull(taskService.getTaskByStatus(TaskStatusEnum.CREATED.getIdTaskStatus()));
    }

    @Test
    public void getAllTask() throws SQLException {
        Assert.assertNotNull(taskService.getAllTask());
    }

    @Test(expected = RuntimeException.class)
    public void getTaskByIdException() throws SQLException {
        taskService.getTaskById(null);
    }

    @Test
    public void getTaskById() throws SQLException {
        Assert.assertEquals(taskService.getTaskById(new Long(1)).size(), 1);
    }
}