package ru.blogic.demo;

import org.apache.openejb.jee.EjbJar;
import org.apache.openejb.junit.ApplicationComposer;
import org.apache.openejb.mockito.MockitoInjector;
import org.apache.openejb.testing.Classes;
import org.apache.openejb.testing.Configuration;
import org.apache.openejb.testing.MockInjector;
import org.apache.openejb.testing.Module;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.blogic.demo.person.model.PersonModel;
import ru.blogic.demo.person.service.PersonService;
import ru.blogic.demo.task.manager.TaskManager;
import ru.blogic.demo.task.model.TaskModel;
import ru.blogic.demo.task.model.TaskStatusEnum;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import static org.mockito.Mockito.when;

@RunWith(ApplicationComposer.class)
public class SqlTaskManagerTest {

    private static final Logger logger = LoggerFactory.getLogger(SqlTaskManagerTest.class);

    @Mock
    PersonService personService;

    @MockInjector
    public Class<?> mockitoInjector() {
        return MockitoInjector.class;
    }

    @Inject
    TaskManager taskManager;

    @Module
    @Classes(cdi = true, value = SqlTaskManager.class)
    public EjbJar init() {
        return new EjbJar();
    }

    @Configuration
    public Properties config() throws Exception {
        Properties p = new Properties();
        p.put("DB_TASK", "new://Resource?type=DataSource");
        p.put("DB_TASK.JdbcDriver", "org.h2.Driver");
        p.put("DB_TASK.JdbcUrl", "jdbc:h2:mem:DB_TASK;INIT=RUNSCRIPT FROM 'classpath:/init.sql';TRACE_LEVEL_FILE=0");
        return p;
    }

    @Before
    public void prepare() {
        PersonModel person = new PersonModel();
        person.setIdPerson(new Long(1));
        person.setName("AO");
        when(personService.getCurrentPerson()).thenReturn(person);
    }

    @Test
    public void addTask() throws SQLException {
        Long idTask = taskManager.addTask("Уйти", null, personService.getCurrentPerson().getIdPerson());
        Assert.assertNotNull(idTask);
    }

    @Test
    public void changeStatus() throws SQLException {
        Long idTask = taskManager.changeStatus(new Long(1), TaskStatusEnum.ARCHIVED.getIdTaskStatus(), personService.getCurrentPerson().getIdPerson());
        Assert.assertNotNull(idTask);
    }

    @Test
    public void getAllTask() throws SQLException {
        List<TaskModel> task = taskManager.getAllTask(personService.getCurrentPerson().getIdPerson());
        Assert.assertNotNull(task);
    }

    @Test
    public void getTaskByStatus() throws SQLException {
        List<TaskModel> task = taskManager.getTaskByStatus(TaskStatusEnum.CREATED.getIdTaskStatus(), personService.getCurrentPerson().getIdPerson());
        Assert.assertNotNull(task);
    }

    @Test
    public void getTaskById() throws SQLException {
        List<TaskModel> task = taskManager.getTaskById(new Long(1), personService.getCurrentPerson().getIdPerson());
        Assert.assertNotNull(task);
    }
}
