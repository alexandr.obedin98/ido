package ru.blogic.demo;

import org.apache.openejb.jee.EjbJar;
import org.apache.openejb.jee.StatelessBean;
import org.apache.openejb.junit.ApplicationComposer;
import org.apache.openejb.mockito.MockitoInjector;
import org.apache.openejb.testing.Classes;
import org.apache.openejb.testing.Configuration;
import org.apache.openejb.testing.MockInjector;
import org.apache.openejb.testing.Module;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.blogic.demo.person.model.PersonModel;
import ru.blogic.demo.person.service.PersonService;
import ru.blogic.demo.task.model.TaskModel;
import ru.blogic.demo.task.model.TaskStatusEnum;
import ru.blogic.demo.task.service.TaskService;

import javax.ejb.EJB;

import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import static org.mockito.Mockito.when;

@RunWith(ApplicationComposer.class)
public class TaskServiceBeanTest {

    final Logger logger = LoggerFactory.getLogger(TaskServiceBeanTest.class);

    @Mock
    PersonService personService;

    @EJB
    TaskService taskService;

    @MockInjector
    public Class<?> mockitoInjector() {
        return MockitoInjector.class;
    }

    @Module
    @Classes(cdi = true, value = SqlTaskManager.class)
    public EjbJar init() {
        EjbJar ejbJar = new EjbJar("beans");
        ejbJar.addEnterpriseBean(new StatelessBean(TaskServiceBean.class));
        return ejbJar;
    }

    @Configuration
    public Properties config() throws Exception {
        Properties p = new Properties();
        p.put("DB_TASK", "new://Resource?type=DataSource");
        p.put("DB_TASK.JdbcDriver", "org.h2.Driver");
        p.put("DB_TASK.JdbcUrl", "jdbc:h2:mem:DB_TASK;INIT=RUNSCRIPT FROM 'classpath:/init.sql';TRACE_LEVEL_FILE=0");
        return p;
    }

    @Before
    public void prepare() {
        PersonModel person = new PersonModel();
        person.setIdPerson(new Long(1));
        person.setName("AO");
        when(personService.getCurrentPerson()).thenReturn(person);
    }

    @Test
    public void setUp() {
    }

    @Test
    public void addTask() throws SQLException {
        Long addTask = taskService.addTask("Test_0", null);
        List<TaskModel> taskList = taskService.getAllTask();
        Assert.assertEquals(addTask, taskList.get(taskList.size() - 1).getIdTask());
    }

    @Test
    public void changeStatus() throws SQLException {
        Long addTask = taskService.addTask("Test_1", null);
        Long changeTask = taskService.changeStatus(addTask, TaskStatusEnum.DELETED.getIdTaskStatus());
        List<TaskModel> deletedTaskList = taskService.getTaskByStatus(TaskStatusEnum.DELETED.getIdTaskStatus());
        Assert.assertEquals(changeTask, deletedTaskList.get(deletedTaskList.size() - 1).getIdTask());
    }

    @Test
    public void getAllTask() throws SQLException {
        addTask();
    }

    @Test
    public void getTaskByStatus() throws SQLException {
        changeStatus();
    }

    @Test
    public void getTaskById() throws SQLException {
        Long addTask = taskService.addTask("Test_1", null);
        List<TaskModel> taskById = taskService.getTaskById(addTask);
        Assert.assertEquals(addTask, taskById.get(0).getIdTask());
    }
}