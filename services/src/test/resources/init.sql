CREATE TABLE IF NOT EXISTS TASK
(
    idTask            BIGSERIAL primary key,
    title             CHARACTER VARYING NOT NULL,
    description       CHARACTER VARYING DEFAULT 'Описание отсутствует',
    idStatus          INT DEFAULT 0 NOT NULL,
    idPerson          CHARACTER VARYING NOT NULL
);

INSERT INTO TASK (title, idPerson)
VALUES ('Задача 1', '1'),
       ('Задача 2', '1'),
       ('Задача 3', '1'),
       ('Задача 4', '2'),
       ('Задача 5', '1');