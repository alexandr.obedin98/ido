package ru.blogic.demo;

import ru.blogic.demo.person.service.PersonService;
import ru.blogic.demo.task.manager.TaskManager;
import ru.blogic.demo.task.model.TaskModel;
import ru.blogic.demo.task.service.TaskService;
import ru.blogic.demo.task.service.impl.TaskServiceImpl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;

public class TaskServiceBean implements TaskService {

    private TaskService taskService;

    @Inject
    private PersonService personService;

    @Inject
    private TaskManager taskManager;

    @PostConstruct
    public void setUp() {
        taskService = new TaskServiceImpl(taskManager, personService);
    }

    @Override
    public Long addTask(String title, String description) throws SQLException {
        return taskService.addTask(title, description);
    }

    @Override
    public Long changeStatus(Long idTask, int idTaskStatus) throws SQLException {
        return taskService.changeStatus(idTask, idTaskStatus);
    }

    @Override
    public List<TaskModel> getAllTask() throws SQLException {
        return taskService.getAllTask();
    }

    @Override
    public List<TaskModel> getTaskByStatus(int idTaskStatus) throws SQLException {
        return taskService.getTaskByStatus(idTaskStatus);
    }

    @Override
    public List<TaskModel> getTaskById(Long idTask) throws SQLException {
        return taskService.getTaskById(idTask);
    }
}
