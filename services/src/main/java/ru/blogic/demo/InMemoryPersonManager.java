package ru.blogic.demo;

import ru.blogic.demo.person.model.PersonModel;
import ru.blogic.demo.person.service.PersonService;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import java.security.Principal;

public class InMemoryPersonManager implements PersonService {

    @Resource
    SessionContext context;

    @Override
    public PersonModel getCurrentPerson() {
        PersonModel person = new PersonModel();

        Principal callerPrincipal = context.getCallerPrincipal();
        if (callerPrincipal != null) {
            person.setIdPerson(new Long(1));
            person.setName(callerPrincipal.getName());
            return person;
        } else {
            return null;
        }
    }
}
