package ru.blogic.demo;

import ru.blogic.demo.task.manager.TaskManager;
import ru.blogic.demo.task.model.TaskModel;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqlTaskManager implements TaskManager {

    @Resource(name = "DB_TASK")
    private DataSource dataSource;

    @Override
    public Long addTask(String title, String description, Long idPerson) throws SQLException {
        Long idTask;
        try (Connection conn = dataSource.getConnection()) {

            String query = "INSERT INTO TASK (TITLE, DESCRIPTION, IDPERSON)\n" +
                    "VALUES (?, ?, ?)";

            PreparedStatement stat = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            stat.setString(1, title);
            stat.setString(2, description);
            stat.setLong(3, idPerson);

            int affectedRows = stat.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating task failed, no rows affected.");
            }

            try (ResultSet generatedKeys = stat.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    idTask = generatedKeys.getLong(1);
                }
                else {
                    throw new SQLException("Creating task failed, no ID obtained.");
                }
            }
        }
        return idTask;
    }

    @Override
    public Long changeStatus(Long idTask, int idTaskStatus, Long idPerson) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {

            String query = "UPDATE TASK\n" +
                    "SET IDSTATUS = ? WHERE IDTASK = ? AND IDPERSON = ?";

            PreparedStatement stat = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            stat.setInt(1, idTaskStatus);
            stat.setLong(2, idTask);
            stat.setLong(3, idPerson);

            int affectedRows = stat.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Update task failed, no rows affected.");
            }

            try (ResultSet generatedKeys = stat.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    idTask = generatedKeys.getLong(1);
                }
                else {
                    throw new SQLException("Update task failed, no ID obtained.");
                }
            }
        }
        return idTask;
    }

    @Override
    public List<TaskModel> getAllTask(Long idPerson) throws SQLException {
        List<TaskModel> taskList = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {

            String query = "SELECT IDTASK, TITLE, DESCRIPTION, IDSTATUS, IDPERSON\n" +
                    "FROM TASK\n" +
                    "WHERE IDPERSON = ?";

            PreparedStatement stat = conn.prepareStatement(query);

            stat.setLong(1, idPerson);

            try (ResultSet resultSet = stat.executeQuery()) {
                while (resultSet.next()) {
                    TaskModel task = new TaskModel();
                    task.setIdTask(resultSet.getLong("IDTASK"));
                    task.setTitle(resultSet.getString("TITLE"));
                    task.setDescription(resultSet.getString("DESCRIPTION"));
                    task.setIdTaskStatus(resultSet.getInt("IDSTATUS"));
                    task.setIdPerson(resultSet.getString("IDPERSON"));
                    taskList.add(task);
                }
            }
        }
        return taskList;
    }

    @Override
    public List<TaskModel> getTaskByStatus(int idTaskStatus, Long idPerson) throws SQLException {
        List<TaskModel> taskList = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {

            String query = "SELECT IDTASK, TITLE, DESCRIPTION, IDSTATUS, IDPERSON\n" +
                    "FROM TASK\n" +
                    "WHERE IDPERSON = ? AND IDSTATUS = ?";

            PreparedStatement stat = conn.prepareStatement(query);

            stat.setLong(1, idPerson);
            stat.setInt(2, idTaskStatus);

            try (ResultSet resultSet = stat.executeQuery()) {
                while (resultSet.next()) {
                    TaskModel task = new TaskModel();
                    task.setIdTask(resultSet.getLong("IDTASK"));
                    task.setTitle(resultSet.getString("TITLE"));
                    task.setDescription(resultSet.getString("DESCRIPTION"));
                    task.setIdTaskStatus(resultSet.getInt("IDSTATUS"));
                    task.setIdPerson(resultSet.getString("IDPERSON"));
                    taskList.add(task);
                }
            }
        }
        return taskList;
    }

    @Override
    public List<TaskModel> getTaskById(Long idTask, Long idPerson) throws SQLException {
        List<TaskModel> taskList = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {

            String query = "SELECT IDTASK, TITLE, DESCRIPTION, IDSTATUS, IDPERSON\n" +
                    "FROM TASK\n" +
                    "WHERE IDPERSON = ? AND IDTASK = ?";

            PreparedStatement stat = conn.prepareStatement(query);

            stat.setLong(1, idPerson);
            stat.setLong(2, idTask);

            try (ResultSet resultSet = stat.executeQuery()) {
                while (resultSet.next()) {
                    TaskModel task = new TaskModel();
                    task.setIdTask(resultSet.getLong("IDTASK"));
                    task.setTitle(resultSet.getString("TITLE"));
                    task.setDescription(resultSet.getString("DESCRIPTION"));
                    task.setIdTaskStatus(resultSet.getInt("IDSTATUS"));
                    task.setIdPerson(resultSet.getString("IDPERSON"));
                    taskList.add(task);
                }
            }
        }
        return taskList;
    }
}
